import {combineReducers} from 'redux';
import allCities from './allCities';
import city from "./city";


const allReducers = combineReducers({

    allCities: allCities,
    city: city
});

export default allReducers;